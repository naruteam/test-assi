import { Component, ViewEncapsulation, Input } from '@angular/core';

@Component({
  selector: 's-mini-avatar',
  templateUrl: './s-mini-avatar.component.html',
  styleUrls: ['./s-mini-avatar.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class SMiniAvatarComponent {
  
    @Input() image: string;
    @Input() first_name: string;
    @Input() last_name: string;

    get initials() {
        if (this.first_name && this.last_name) {
            return this.first_name.charAt(0).toUpperCase() + '' + this.last_name.charAt(0).toUpperCase();
        } else {
            return '';
        }
    }
}