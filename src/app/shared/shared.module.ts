import { NgModule } from '@angular/core';
import { SCardComponent } from './2_molecules/s-card/s-card.component';
import { CommonModule } from '@angular/common';
import { SMiniAvatarComponent } from './2_molecules/s-mini-avatar/s-mini-avatar.component';
import { HttpClientModule } from '@angular/common/http';
import { ProfileService } from '../services/profile.service';
import { AccountService } from '../services/account.service';

@NgModule({
  declarations: [
    SCardComponent,
    SMiniAvatarComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule
  ],
  exports: [
    SCardComponent,
    SMiniAvatarComponent
  ],
  providers: [
    ProfileService,
    AccountService
  ]  
})
export class SharedModule { }