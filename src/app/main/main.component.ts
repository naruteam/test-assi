import { Component, OnInit } from '@angular/core';
import { MainService } from './main.service';
import { ProfileService } from '../services/profile.service';

@Component({
  selector: 'main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  id: number = 2;

  constructor(private mainService: MainService) {
  }

  ngOnInit() {
    this.mainService.getProfile(this.id);
    this.mainService.getAccounts();
  }
}
