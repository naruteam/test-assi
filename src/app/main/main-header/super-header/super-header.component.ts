import { Component, OnInit } from '@angular/core';
import { MainService } from '../../main.service';

@Component({
  selector: 'super-header',
  templateUrl: './super-header.component.html',
  styleUrls: ['./super-header.component.css']
})
export class SuperHeaderComponent implements OnInit {
  
  profile: any;

  constructor(private mainService: MainService) {
  }

  ngOnInit() {
    this.profile = this.mainService.profile;
  }

  get fullName() {
    return this.profile.first_name + ' ' + this.profile.last_name;
  }

  editStore() {
    this.mainService.editStore();
  }

  clickAvatar() {
    this.mainService.clickAvatar();
  }

  print() {    
    this.mainService.print();
  }
}
