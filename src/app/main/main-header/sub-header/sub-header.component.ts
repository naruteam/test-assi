import { Component } from '@angular/core';
import { MainService } from '../../main.service';

@Component({
  selector: 'sub-header',
  templateUrl: './sub-header.component.html',
  styleUrls: ['./sub-header.component.css']
})
export class SubHeaderComponent {

  constructor(private mainService: MainService) {    
  }

  action(name) {   
    this.mainService.subHeaderButton(name);
  }
}
