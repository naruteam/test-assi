import { Component, OnInit } from '@angular/core';
import { MainService } from '../main.service';

@Component({
  selector: 'main-body',
  templateUrl: './main-body.component.html',
  styleUrls: ['./main-body.component.css']
})
export class MainBodyComponent implements OnInit {
  
  accounts: Array<any> = [];
  profile: any;
  view_account_id: number = 0;

  constructor(private mainService: MainService) {    
  }

  ngOnInit() {
    this.profile = this.mainService.profile;
    this.accounts = this.mainService.accounts;
  }

  getTrea(account) {
    return 'S/ ' + account.trea_sols + '% y $ ' + account.trea_dollars + '%';
  }

  getOpeningAmount(account) {
    return 'S/ ' + account.opening_amount_sols + '' 
      + (account.opening_amount_sols > 0? (' o $ ' + account.opening_amount_dollars): '');
  }

  viewAccount(account) {
    if (this.view_account_id != account.id) {
      this.view_account_id = account.id;
    } else {
      this.view_account_id = 0;
    }
  }
}
