import { Injectable } from '@angular/core';
import { ProfileService } from '../services/profile.service';
import { AccountService } from '../services/account.service';

@Injectable()
export class MainService {

    profile: any = {};
    accounts: Array<any> = [];

    constructor(private profileService: ProfileService, private accountService: AccountService) {
    }

    editStore() {
        console.log("Edit Store..")
    }

    clickAvatar() {
        console.log("Avatar Clicked..");
    }

    print() {
        console.log("Printing..");        
    }

    subHeaderButton(action) {

        if (action == 'dashboard') {
            console.log("Go Dashboard..");
        }
        if (action == 'exit') {
            console.log("Exit..");
        }
    }

    getProfile(id) {        
        this.profileService.getOne(id).subscribe((data) => {                        
            Object.assign(this.profile, data);            
        });        
    }

    getAccounts() {        
        this.accountService.getAll().subscribe((data) => {
            for (let key in data) {
                this.accounts.push(data[key]);
            }         
        });        
    }
}