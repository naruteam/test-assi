import { NgModule } from '@angular/core';
import { MainComponent } from './main.component';
import { MainHeaderComponent } from './main-header/main-header.component';
import { SuperHeaderComponent } from './main-header/super-header/super-header.component';
import { SubHeaderComponent } from './main-header/sub-header/sub-header.component';
import { MainBodyComponent } from './main-body/main-body.component';
import { SharedModule } from '../shared/shared.module';
import { CommonModule } from '@angular/common';
import { MainService } from './main.service';

@NgModule({
  declarations: [    
    MainComponent,
    MainHeaderComponent,
    SuperHeaderComponent,
    SubHeaderComponent,
    MainBodyComponent
  ],
  imports: [
    CommonModule,
    SharedModule,    
  ],
  exports: [
    MainComponent,
    MainHeaderComponent,
    SuperHeaderComponent,
    SubHeaderComponent,
    MainBodyComponent
  ],
  providers: [
    MainService
  ]  
})
export class MainModule { }