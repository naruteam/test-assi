import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';

@Injectable()
export class AccountService {

    constructor(private http: HttpClient) {        
    }

    getAll() {
        return this.http.get(environment.baseUrl + '/accounts');
    }
}