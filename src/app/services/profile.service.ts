import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';

@Injectable()
export class ProfileService {

    constructor(private http: HttpClient) {        
    }

    getOne(id) {
        return this.http.get(environment.baseUrl + '/profiles/' + id);
    }
}